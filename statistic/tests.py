from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.test import TestCase
from .views import index
# Create your tests here.
class StatsUnitTest(TestCase):

    def test_stats_url_is_exist(self):
        response = Client().get('/statistic/')
        self.assertEqual(response.status_code, 200)

    def test_stats_using_index_func(self):
        found = resolve('/statistic/')
        self.assertEqual(found.func, index)
