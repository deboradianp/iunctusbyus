from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import AddFriend

# Create your views here.
response = {}
def index(request):
    todo = AddFriend.objects.all()
    response['todo'] = todo
    html = 'friends.html'
    response['friend_form'] = Friend_Form
    return render(request, html, response)

def add_friend(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        todo = AddFriend(name=request.POST['name'],url=request.POST['url'])
        todo.save()
        return HttpResponseRedirect('/friend')
