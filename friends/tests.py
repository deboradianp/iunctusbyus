from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import AddFriend
from .views import index

# Create your tests here.
class FriendUnitTest(TestCase):
	def test_friend_url_is_exist(self):
		response = Client().get('/friends/')
		self.assertEqual(response.status_code, 200)

	def test_friend_using_index_func(self):
		found = resolve('/friends/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_friend(self):
        #Creating a new activity
		new_activity = AddFriend.objects.create(name="afwan",url='heroku.com')

        #Retrieving all available activity
		counting_all_available_friend= AddFriend.objects.all().count()
		self.assertEqual(counting_all_available_friend,1)

	def test_addfriend_post_success_and_render_the_result(self):
		anonymous = 'Anonymous'
		url = 'herokuapp.com'
		response = Client().post('/friends/add_friend/', {'name': anonymous, 'url': url})
		self.assertEqual(response.status_code, 302)
		response = Client().get('/friends/')
		html_response = response.content.decode('utf8')
		self.assertIn(anonymous,html_response)
