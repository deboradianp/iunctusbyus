from django.db import models
from django.utils import timezone
import pytz

# Create your models here.
class AddFriend(models.Model):
	def converTZ():
		return timezone.now() + timezone.timedelta(hours=0)

	name = models.CharField(max_length=27)
	url = models.URLField(max_length=27)
	created_date = models.DateTimeField(default=converTZ)
