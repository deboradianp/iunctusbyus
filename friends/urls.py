from django.conf.urls import url
from .views import index, add_friend
#from .views import add_activity

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_friend/', add_friend, name='add_friend'),

]
