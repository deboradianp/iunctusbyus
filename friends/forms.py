from django import forms

class Friend_Form(forms.Form):
	error_messages = {
		'required' : 'Isi input ini',
		'invalid' : 'Isi input ini dengan url'
	}

	name_attrs={
		'type': 'text',
        'class': 'todo-form-input-nameFriend',
        'placeholder':'Nama kamu...'
	}

	url_attrs={
		'type': 'url',
        'class': 'todo-form-input-urlFriend',
        'placeholder':'Url...'
	}

	name = forms.CharField(label='Name', required=True, max_length=100, empty_value='Anonymous', widget=forms.TextInput(attrs=name_attrs))
	url = forms.URLField(label='URL', required=True,max_length=100, empty_value='Anonymous', widget=forms.URLInput(attrs=url_attrs))
